define(['text!news/news-search.html', '../base/util', "../base/openapi", "../tweet/tweet-list", "../news/news-list", "../question/question-list"],
	function(viewTemplate, Util, OpenAPI, TweetList, NewsList, QuestionList) {
		return Piece.View.extend({
			tweetList: null,
			newsList: null,
			questionList: null,
			id: 'news_news-search',
			events: {
				"click .left-banner": "goHome",
				"click #searchButton": "search",
				"click .projectList": "goDetail"
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var searchContent = Piece.Store.loadObject("software-search");
				/*利用local storage加载搜索条件*/
				if (searchContent != null && searchContent !== "") {
					$(".newsSearchInput").val(searchContent);
					this.search();
				}else {
					$(".newsSearchInput").focus();
				}
				var me = this;
				$(me.el).find("li").bind('click', function() {
					$(me.el).find('.active').removeClass('active');
					$(this).addClass("active");
					me.search();
				});
			},
			goHome: function() {
				window.history.back();

			},
			search: function() {
				var me = this;

				var searchType = $(".segmented-controller").find(".active").attr("data-type");
				var searchContent = $(".newsSearchInput").val();
				var searchContentNew = searchContent.replace(/(\s*$)/g, ""); //去掉右边空格
				Piece.Store.saveObject("software-search", searchContent); /*将搜索信息保存，下次进来先从本地加载搜索信息*/
				if (searchContentNew === "" || searchContentNew === " ") {
					new Piece.Toast("搜索内容不能为空");
				} else {
					$('#news-news-searchList').css("display","block");
					Util.loadList(me, 'news-news-searchList', OpenAPI.news_search, {
						'catalog': searchType,
						"q": searchContent,
						'dataType': OpenAPI.dataType,
						'page': 1,
						'pageSize': OpenAPI.pageSize
					},true);
				}
			},
			goDetail: function(el) {
				tweetList = new TweetList();
				newsList = new NewsList();
				questionList = new QuestionList();

				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				var objType = $target.attr("data-objType");
				var url = $target.attr("data-url");

				/*-------------------传到详情页面参数-----------------------*/
				var fromType; //1-软件；2-问答；3-博客；4-咨询；5-代码；7-翻译；
				var checkDetail;
				var com; //1-新闻；2-问答；3-动弹；5-博客；
				console.info(objType)
				if (objType == "news") {
					fromType = 4;
					checkDetail = "news/news-detail";
					com = 1;
					//判断返回  到我的空间还是  不同的DETAUIL详情
					from = null;
					newsList.navigate("news/news-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
						trigger: true
					});
				} else if (objType == "project") {
					this.navigateModule("project/software-detail?url=" + url, {
						trigger: true
					});
				} else if (objType == "blog") {
					fromType = 3;
					checkDetail = "news/news-blog-detail";
					com = 5;
					//判断返回  到我的空间还是  不同的DETAUIL详情
					from = null;

					newsList.navigate("news/news-blog-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
						trigger: true
					});
				} else if (objType == "post") {
					fromType = 2;
					checkDetail = "question/question-detail";
					com = 2;
					from = null;

					questionList.navigate("question/question-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
						trigger: true
					});
				}

			},
		}); //view define

	});